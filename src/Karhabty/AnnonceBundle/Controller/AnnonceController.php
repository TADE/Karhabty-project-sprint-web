<?php

namespace Karhabty\AnnonceBundle\Controller;

use Karhabty\AnnonceBundle\Entity\Annonce;
use Karhabty\AnnonceBundle\Entity\Commentaire;
use Karhabty\AnnonceBundle\Entity\Contacte;
use Karhabty\AnnonceBundle\Entity\Modeles;
use Karhabty\AnnonceBundle\Form\AnnonceType;
use Karhabty\AnnonceBundle\Form\CommentaireType;
use Karhabty\AnnonceBundle\Form\ContacteType;
use Karhabty\AnnonceBundle\Form\FilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AnnonceController extends Controller
{
    public function getModelByMarqueAction($id) {
        $em = $this->getDoctrine()->getManager();
        $mod = $em->getRepository('KarhabtyAnnonceBundle:Modeles')->findBy(
            array('idmarque' => $id)
        );
        if ($mod) {
            $tab = [];
            $i = 0;
            foreach ($mod as $Tabmod) {
                $tab[$i] = $Tabmod->getNom();
                $i++;
            }
        }
        else {
            $tab = null;
        }
        $response = new JsonResponse();
        return $response->setData(array('model'=>$tab));
    }
    public function AllAnnonceAction(Request $req) {
        set_time_limit(500);
        $annon = new Annonce();
        $form = $this->createForm(FilterType::class,$annon);
        $em = $this->getDoctrine()->getManager();
        if ($form->handleRequest($req)->isValid()) {
            $annon->setModele($req->get('sel'));
            $ann = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->FilterAnnonce($annon->getMarque(),$annon->getModele(),$annon->getEnergie(),$annon->getBoite());
            $paginator = $this->get('knp_paginator')->paginate($ann,$req->query->get('page',1),10);
            return $this->render('KarhabtyAnnonceBundle:FrontOffice:ListAnnonce.html.twig',array('ann'=>$paginator,'ft'=>$form->createView()));
        }
        $ann = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->findAll();
        $paginator = $this->get('knp_paginator')->paginate($ann,$req->query->get('page',1),10);
        return $this->render('KarhabtyAnnonceBundle:FrontOffice:ListAnnonce.html.twig',array('ann'=>$paginator,'ft'=>$form->createView()));
    }
    public function DisplayAnnonceAction(Request $req,$id)
    {
        set_time_limit(500);
        $annon = new Annonce();
        $form = $this->createForm(FilterType::class,$annon);
        $em = $this->getDoctrine()->getManager();
        $fav = $em->getRepository('KarhabtyAnnonceBundle:Favorie')->findBy(array('utilisateurutilisateur'=>$id));
        if ($form->handleRequest($req)->isValid()) {
            $annon->setModele($req->get('sel'));
            $ann = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->FilterAnnonce($annon->getMarque(),$annon->getModele(),$annon->getEnergie(),$annon->getBoite(),$id);
            $paginator = $this->get('knp_paginator')->paginate($ann,$req->query->get('page',1),10);
            return $this->render('KarhabtyAnnonceBundle:FrontOffice:ListAnnonce.html.twig',array('ann'=>$paginator,'fav'=>$fav,'ft'=>$form->createView()));
        }
        $ann = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->AllAnnonce($id);
        $paginator = $this->get('knp_paginator')->paginate($ann,$req->query->get('page',1),2);

        return $this->render('KarhabtyAnnonceBundle:FrontOffice:ListAnnonce.html.twig',array('ann'=>$paginator,'fav'=>$fav,'ft'=>$form->createView()));
    }
    public function AjouterAnnonceAction(Request $req,$id)
    {
        set_time_limit(1000);
        $annonce = new Annonce();
        $form = $this->createForm(AnnonceType::class,$annonce);
        if ($form->handleRequest($req)->isValid()) {
            $annonce->setModele($req->get('sel'));
            $em = $this->getDoctrine()->getManager();
            $user=$em->getRepository('KarhabtyUserBundle:Users')->find($id);
            $annonce->setIduser($user);
            $do = new \DateTime();
            $annonce->setDateannonce($do);
            $alert = $em->getRepository('KarhabtyAnnonceBundle:Alert')->GetAlertExiste($annonce->getMarque(),$annonce->getModele());
            if ($alert) {
                foreach ($alert as $data) {
                    $mailuser = $em->getRepository('KarhabtyUserBundle:Users')->find($data->getUtilisateurutilisateur());
                    $msg = \Swift_Message::newInstance()
                        ->setSubject("Alert trouvé")
                        ->setFrom(array('taher.derbel@esprit.tn'=>'Taher Derbel'))
                        ->setTo($mailuser->getEmail())
                        ->setBody($this->renderView('KarhabtyAnnonceBundle:Default:mailAlert.html.twig'));
                    $this->get('mailer')->send($msg);
                    $dropalert = $em->getRepository('KarhabtyAnnonceBundle:Alert')->find($data->getIdalert());
                    $em->remove($dropalert);
                    $em->flush();
                }
            }
            $em->persist($annonce);
            $em->flush();
            $notif = $em->getRepository('KarhabtyAnnonceBundle:Notif')->find(1);
            $notif->setNbrannonce($notif->getNbrannonce()+1);
            $em->persist($notif);
            $em->flush();
            return $this->render('KarhabtyAnnonceBundle:FrontOffice:AjouterAnnonce.html.twig',array('f'=>$form->createView()));
        }
        return $this->render('KarhabtyAnnonceBundle:FrontOffice:AjouterAnnonce.html.twig',array('f'=>$form->createView()));
    }
    public function DisplayMyAnnonceAction(Request $req,$id)
    {
        set_time_limit(500);
        $em = $this->getDoctrine()->getManager();
        $Annonce = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->findBy(array('iduser'=>$id));
        $num = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->Count($id);
        $paginator = $this->get('knp_paginator')->paginate($Annonce,$req->query->get('page',1),10);
        return $this->render('KarhabtyAnnonceBundle:FrontOffice:MesAnnonce.html.twig',array('ann'=>$paginator,'num'=>$num));
    }
    public function DeleteAnnonceAction(Request $req,$id) {
        set_time_limit(500);
        $em = $this->getDoctrine()->getManager();
        $Annonce = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->find($req->get('id2'));
        $em->remove($Annonce);
        $em->flush();
        return $this->redirectToRoute('karhabty_mes_annonce',array('id'=>$id));
    }
    public function UpdateAnnonceAction(Request $req,$id) {
        set_time_limit(500);
        $em = $this->getDoctrine()->getManager();
        $Annonce = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->find($id);
        $form = $this->createForm(AnnonceType::class,$Annonce);
        if ($form->handleRequest($req)->isValid()) {
            $em->persist($Annonce);
            $em->flush();
            return $this->render('KarhabtyAnnonceBundle:FrontOffice:ModifAnnonce.html.twig',array('f'=>$form->createView()));
        }
        return $this->render('KarhabtyAnnonceBundle:FrontOffice:ModifAnnonce.html.twig',array('f'=>$form->createView()));
    }
    public function DetailAnnonceAction(Request $req,$id) {
        set_time_limit(1000);
        $contact = new Contacte();
        $comment = new Commentaire();
        $form2 = $this->createForm(CommentaireType::class,$comment);
        $form = $this->createForm(ContacteType::class,$contact);
        $em = $this->getDoctrine()->getManager();
        $Annonce = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->find($req->get('id2'));
        $ven = $em->getRepository('KarhabtyUserBundle:Users')->find($Annonce->getIduser());
        $sim = $em->getRepository('KarhabtyAnnonceBundle:Annonce')->SimilarAnnonce($Annonce->getMarque(),$Annonce->getModele(),$Annonce->getIdannonce(),$id);
        $comm = $em->getRepository('KarhabtyAnnonceBundle:Commentaire')->findBy(array('idAnnonce'=>$req->get('id2')));
        $numcomm = $em->getRepository('KarhabtyAnnonceBundle:Commentaire')->Count($req->get('id2'));
        $all = $em->getRepository('KarhabtyUserBundle:Users')->findAll();
        if ($form->handleRequest($req)->isValid()) {
            $source = $em->getRepository('KarhabtyUserBundle:Users')->find($id);
            $msg = \Swift_Message::newInstance()
                ->setSubject("Contact")
                ->setFrom(array('chikitos.tunisie@gmail.com'=>'Karhabty'))
                ->setTo($ven->getEmail())
                ->setBody($this->renderView('KarhabtyAnnonceBundle:Default:mailContact.html.twig',array('contenu'=>$contact->getContenu())));
            $this->get('mailer')->send($msg);
            $contact->setNomsource($source->getFirstName()."".$source->getLastName());
            $contact->setEmaildest($ven->getEmail());
            $do = new \DateTime();
            $contact->setDate($do);
            $contact->setUtilisateurutilisateur($source);
            $em->persist($contact);
            $em->flush();
            return $this->render('KarhabtyAnnonceBundle:FrontOffice:AnnonceDetail.html.twig',array('ann'=>$Annonce,'ven'=>$ven,'sim'=>$sim,'f'=>$form->createView(),'f2'=>$form2->createView(),'comm'=>$comm,'num'=>$numcomm,'all'=>$all));
        }

        if ($form2->handleRequest($req)->isValid()) {
            $source = $em->getRepository('KarhabtyUserBundle:Users')->find($id);
            $comment->setNom($source->getFirstName());
            $do = new \DateTime();
            $comment->setDatecommentaire($do);
            $comment->setUtilisateurutilisateur($source);
            $comment->setIdAnnonce($Annonce);
            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute('karhabty_detail_annonce',array('id2'=>$req->get('id2'),'id'=>$id));
        }
        return $this->render('KarhabtyAnnonceBundle:FrontOffice:AnnonceDetail.html.twig',array('ann'=>$Annonce,'ven'=>$ven,'sim'=>$sim,'f'=>$form->createView(),'f2'=>$form2->createView(),'comm'=>$comm,'num'=>$numcomm,'all'=>$all));
    }

}
