<?php
/**
 * Created by PhpStorm.
 * User: iheb
 * Date: 22/03/2017
 * Time: 17:50
 */

namespace Karhabty\ForumBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="Karhabty\ForumBundle\Repository\ForumRepository")
 */
class Topics
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;
    /**
     * @ORM\Column(type="string",length=50)
     */
    private $title;
    /**
     * @ORM\Column(type="text")
     */
    private $content;
    /**
     * @ORM\Column(type="string",length=150)
     */
    private $description;
    /**
     * @ORM\Column(type="datetime")
     */
    private $date;
    /**
     * @ORM\Column( type="boolean", nullable=true)
     */
    private $resolved;
    /**
     * @ORM\Column( type="boolean", nullable=true)
     */
    private $closed;
    /**
     * @ORM\Column(type="string",length=50)
     */
    private $slug;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastpost;
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category",referencedColumnName="id",onDelete="CASCADE")
     */
    private $category;
    /**
     * @ORM\ManyToOne(targetEntity="Karhabty\UserBundle\Entity\Users")
     * @ORM\JoinColumn(name="owner",referencedColumnName="id",onDelete="CASCADE")
     */
    private $owner;
    /**
     * @ORM\Column( type="integer", nullable=true)
     */
    private $likes;
    /**
     * @ORM\Column( type="integer", nullable=true)
     */
    private $views;
    /**
     * Topics constructor.
     */
    public function __construct()
    {
        $this->closed=false;
        $this->resolved=false;
        $this->likes=0;
        $this->date=new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getResolved()
    {
        return $this->resolved;
    }

    /**
     * @param mixed $resolved
     */
    public function setResolved($resolved)
    {
        $this->resolved = $resolved;
    }

    /**
     * @return mixed
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * @param mixed $closed
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getLastpost()
    {
        return $this->lastpost;
    }

    /**
     * @param mixed $lastpost
     */
    public function setLastpost($lastpost)
    {
        $this->lastpost = $lastpost;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param mixed $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }


}