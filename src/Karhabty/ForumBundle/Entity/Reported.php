<?php
/**
 * Created by PhpStorm.
 * User: iheb
 * Date: 24/03/2017
 * Time: 18:27
 */

namespace Karhabty\ForumBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class Reported
{
    /**
     * @ORM\Column(type="string",length=50)
     */
    private $reason;
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Karhabty\UserBundle\Entity\Users", inversedBy="reportedpost")
     * @ORM\JoinColumn(name="reporter", referencedColumnName="id", nullable=false)
     */
    private $reporter;
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="reportedpost")
     * @ORM\JoinColumn(name="reportedpost", referencedColumnName="id", nullable=false)
     */
    private $reprotedpost;

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * @param mixed $reporter
     */
    public function setReporter($reporter)
    {
        $this->reporter = $reporter;
    }

    /**
     * @return mixed
     */
    public function getReprotedpost()
    {
        return $this->reprotedpost;
    }

    /**
     * @param mixed $reprotedpost
     */
    public function setReprotedpost($reprotedpost)
    {
        $this->reprotedpost = $reprotedpost;
    }

}