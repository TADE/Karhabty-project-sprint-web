<?php
/**
 * Created by PhpStorm.
 * User: iheb
 * Date: 22/03/2017
 * Time: 17:51
 */

namespace Karhabty\ForumBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="Karhabty\ForumBundle\Repository\ForumRepository")
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;
    /**
     * @ORM\Column(type="text")
     */
    private $content;
    /**
     * @ORM\Column(type="datetime")
     */
    private $date;
    /**
     * @ORM\ManyToOne(targetEntity="Topics")
     * @ORM\JoinColumn(name="topic",referencedColumnName="id",onDelete="CASCADE")
     */
    private $topic;
    /**
     * @ORM\ManyToOne(targetEntity="Karhabty\UserBundle\Entity\Users")
     * @ORM\JoinColumn(name="poster",referencedColumnName="id",onDelete="CASCADE")
     */
    private $poster;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;
    /**
     * @ORM\Column( type="integer", nullable=true)
     */
    private $likes;
    /**
     * @ORM\ManyToOne(targetEntity="Post")
     * @ORM\JoinColumn(name="response",referencedColumnName="id",onDelete="CASCADE")
     */
    private $response;
    /** @ORM\OneToMany(targetEntity="Karhabty\UserBundle\Entity\Users", mappedBy="Post") */
    private $reportedpost;


    /**
     * Post constructor.
     * @param $date
     */
    public function __construct()
    {
        $this->date = new \DateTime();
        $this->likes=0;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param mixed $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @return mixed
     */
    public function getPoster()
    {
        return $this->poster;
    }

    /**
     * @param mixed $poster
     */
    public function setPoster($poster)
    {
        $this->poster = $poster;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getReportedpost()
    {
        return $this->reportedpost;
    }

    /**
     * @param mixed $reportedpost
     */
    public function setReportedpost($reportedpost)
    {
        $this->reportedpost = $reportedpost;
    }

}