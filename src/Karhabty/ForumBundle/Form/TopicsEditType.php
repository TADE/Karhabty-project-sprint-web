<?php

namespace Karhabty\ForumBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TopicsEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title')->add('description', TextareaType::class)->add('category',EntityType::class,array(
            'class'=>'Karhabty\ForumBundle\Entity\Category','choice_label'=>'name'
        ))->add('save',SubmitType::class) ->add('content',TextareaType::class,array('attr' => array('class' => 'ckeditor')))
            ->add('closed',CheckboxType::class,array('label' => 'Close This Topic ','required' => false))
            ->add('resolved',CheckboxType::class,array('label' => 'This Topic is Resolved ','required' => false));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Karhabty\ForumBundle\Entity\Topics'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'karhabty_forumbundle_topics';
    }


}
