<?php

namespace Karhabty\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('KarhabtyForumBundle:Default:index.html.twig');
    }
}
