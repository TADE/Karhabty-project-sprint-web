<?php

namespace Karhabty\ForumBundle\Controller;

use Karhabty\ForumBundle\Entity\Post;
use Karhabty\ForumBundle\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }
    public function PostReplyAction($id,Request $request)
    {
        $post=new Post();
        $form=$this->createForm(PostType::class,$post);
        $em=$this->getDoctrine()->getManager();
        $postt=$em->getRepository('KarhabtyForumBundle:Post')->findOneBy(array('id'=>$id));
        $reply=$em->getRepository('KarhabtyForumBundle:Post')->findPostReply($postt);
        $slugc=$postt->getTopic()->getCategory()->getSlug();
        $slugt=$postt->getTopic()->getSlug();
        if ($form->handleRequest($request)->isValid())
        {
            $post->setResponse($postt);
            $post->setPoster($this->getUser());
            $post->setTopic($postt->getTopic());
            $em->persist($post);
            $em->flush();
            $redirect=$this->generateUrl('karhabty_forum_Topic_Details',array('slugc'=>$slugc,'slugt'=>$slugt));
            return $this->redirect($redirect);

        }
        return $this->render('@KarhabtyForum/Post/PostReply.html.twig',array('replyform'=>$form->createView(),'post'=>$postt,'reply'=>$reply));
    }
    public function PostEditAction($id,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $post=$em->getRepository('KarhabtyForumBundle:Post')->findOneBy(array('id'=>$id));
        $form=$this->createForm(PostType::class,$post);
        $slugc=$post->getTopic()->getCategory()->getSlug();
        $slugt=$post->getTopic()->getSlug();
        if ($form->handleRequest($request)->isValid())
        {
            $post->setUpdated(new \DateTime());
            $em->persist($post);
            $em->flush();
            $redirect=$this->generateUrl('karhabty_forum_Topic_Details',array('slugc'=>$slugc,'slugt'=>$slugt));
            return $this->redirect($redirect);
        }
        return $this->render('@KarhabtyForum/Post/EditPost.html.twig',array('editform'=>$form->createView()));
    }
    public function PostDeleteAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $post=$em->getRepository('KarhabtyForumBundle:Post')->findOneBy(array('id'=>$id));
        $slugc=$post->getTopic()->getCategory()->getSlug();
        $slugt=$post->getTopic()->getSlug();
            $em->remove($post);
            $em->flush();
            $redirect=$this->generateUrl('karhabty_forum_Topic_Details',array('slugc'=>$slugc,'slugt'=>$slugt));
            return $this->redirect($redirect);

    }
    public function LikePostAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $post=$em->getRepository('KarhabtyForumBundle:Post')->findOneBy(array('id'=>$id));
        $post->setLikes($post->getLikes()+1);
        $em->persist($post);
        $em->flush();
        $reponse= new JsonResponse();
        return $reponse->setData(array('resultat'=>$post->getLikes()));

    }
    public function DislikePostAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $post=$em->getRepository('KarhabtyForumBundle:Post')->findOneBy(array('id'=>$id));
        $post->setLikes($post->getLikes()-1);
        $em->persist($post);
        $em->flush();
        $reponse= new JsonResponse();
        return $reponse->setData(array('resultat'=>$post->getLikes()));

    }
    public function DisplayPostsAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $posts=$em->getRepository('KarhabtyForumBundle:Post')->findAll();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($posts, $request->query->get('page', 1)/*page number*/, 10/*limit per page*/);
        return $this->render('@KarhabtyAdmin/Forum/DisplayPost.html.twig',array('topics'=>$pagination));
    }
}
