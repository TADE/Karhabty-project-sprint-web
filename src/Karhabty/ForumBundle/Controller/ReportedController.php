<?php

namespace Karhabty\ForumBundle\Controller;

use Karhabty\ForumBundle\Entity\Reported;
use Karhabty\ForumBundle\Form\ReportedType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReportedController extends Controller
{
    public function ReportedAction(Request $request,$id)
    {
        $reported=new Reported();
        $form=$this->createForm(ReportedType::class,$reported);
        $em=$this->getDoctrine()->getManager();
        $post=$em->getRepository('KarhabtyForumBundle:Post')->findOneBy(array('id'=>$id));
        $slugc=$post->getTopic()->getCategory()->getSlug();
        $slugt=$post->getTopic()->getSlug();
        if ($form->handleRequest($request)->isValid())
        {
            $reported->setReporter($this->getUser());
            $reported->setReprotedpost($post);
            $em->persist($reported);
            $em->flush();
            $redirect=$this->generateUrl('karhabty_forum_Topic_Details',array('slugc'=>$slugc,'slugt'=>$slugt));
            return $this->redirect($redirect);

        }
        return $this->render('@KarhabtyForum/Reported/Report.html.twig', array('form'=>$form->createView()));
    }
    public function ReportedPostsAction()
    {
        $em=$this->getDoctrine()->getManager();
        $reported=$em->getRepository('KarhabtyForumBundle:Reported')->findAll();
        return $this->render('@KarhabtyAdmin/Forum/DisplayReportedPosts.html.twig',array('reported'=>$reported));
    }
    public function SendEmailToReporterAction(Request $request)
    {
        if ($request->isMethod('POST'))
        {
            $email=$request->get('email');
            $subject=$request->get('subject');
            $postcotent=$request->get('postcontent');
            $username=$request->get('username');
            $content=$request->get('content');
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom(array('chikitos.tunisie@gmail.com' => "Karhabty"))
                ->setTo($email)
                ->setBody(
                    $this->renderView('@KarhabtyForum/Emails/SendEmailToReporter.html.twig', array('name' => $username,'postcontent'=>$postcotent,'content'=>$content)), 'text/html');
            $this->get('mailer')->send($message);
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'L\'Email a ete envoyer');
            return $this->redirectToRoute('karhabty_admin_Reported_Posts');
        }
        return $this->redirectToRoute('karhabty_admin_Reported_Posts');
    }

}
